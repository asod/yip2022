Proof-of-concept calculations for the XDS-Enriched MLPs
=======================================================

Implements the XDS-Forces and Energy as an `ASE`-Calculator, 
tests the implementation of the energies and forces, and shows how the forces can be used as a regularization-term in a future loss-function for MLP-training, encoding direct structural information into the optimization procedure. 

See `XDS-MLP.ipynb`, which produces

1: A plot showing the relationship between calculated Debye-scattering and the XDS-Energies:
![XDS-Potential](xds-pot.png)

2: A plot showing the XDS-Force relationship between a model set of "good" structures (as a molecular dynamics trajectory of a diatomic), and the "bad" structures, representing the results from an un-enriched MLP
![XDS-Forces](plot2.png)



Requirements:
--------------- 
Numpy, Matplotlib, ASE, numba, all available through `pip`.





