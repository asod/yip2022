import numpy as np
from ase import Atoms
from ase.calculators.calculator import Calculator, all_changes
from debye import Debye

class DebyeCalc(Calculator):
    ''' ASE-calculator with forces from Debye-scattering.
        takes debye class above '''
    implemented_properties = ['energy', 'forces']

    def __init__(self, debye, s_exp=None, update=False, k=1, alpha=1, sigma=None):
        Calculator.__init__(self)
        self.debye = debye
        self.s_exp = s_exp  # what to hit. If none, the calculator expects a trajectory:
        self.update = update  # put in a trajectory, and s_exp will be updated with scattering from each step in this trajectory. 
        self.step = 0
        self.k = k                        # user defined force constant
        self.alpha = alpha                # excitation fraction
        if sigma is not None:             # weighs
            self.sigma = sigma
        else:
            self.sigma = np.ones_like(debye.qvec)

    def calculate(self, atoms=None,
                  properties=['energy', 'forces'],
                  system_changes=all_changes):
        Calculator.calculate(self, atoms, properties, system_changes)

        s_exp = self.s_exp
        if self.update:
           s_exp = self.debye.debye(self.update[self.step]) 
           self.step += 1

        e = self.xds_epot(atoms, s_exp)
        f = self.xds_f_fast(atoms, s_exp)

        self.results['energy'] = e
        self.results['forces'] = f

    def xds_epot(self, atoms, s_exp):
        ''' Energy from scattering difference '''
        s_sim = self.debye.debye(atoms)

        weighs = 1 / (self.sigma**2)
        u_xds = 0.5 * self.k * np.sum(weighs * (s_exp - s_sim)**2)

        return u_xds

    def Aq(self, q, s_sim, s_exp):
        return (self.k / (2 * self.sigma[q]**2)) * (s_exp[q] - s_sim[q])

    def xds_f(self, atoms, s_exp):
        ''' A la Bjorling et al + Chen et al.
        '''
        s_sim = self.debye.debye(atoms)
        r_kj = atoms.get_all_distances()
        r_kj2 = r_kj**2
        pos = atoms.get_positions()

        f = np.zeros((len(atoms), 3))
        for k, atm_k in enumerate(atoms.get_chemical_symbols()):
            f0_k = self.debye.atomic_f0(atm_k)
            for q, qq in enumerate(self.debye.qvec):
                A = self.Aq(q, s_sim, s_exp)
                for j, atm_j in enumerate(atoms.get_chemical_symbols()):
                    if k == j:
                        continue
                    f0_j = self.debye.atomic_f0(atm_j)
                    qr = qq * r_kj[k, j]
                    f[k, :] += 4 * A *\
                               (f0_k[q] * f0_j[q] *  # noqa: E127
                               (np.cos(qr) - np.sin(qr) / qr) *  # noqa: E128
                               (pos[k] - pos[j]) / r_kj2[k, j])  # noqa: E128
        return f

    def xds_f_fast(self, atoms, s_exp):
        ''' vectorized xds_f. '''

        s_sim = self.debye.debye(atoms)
        pos = atoms.get_positions()
        syms = atoms.get_chemical_symbols()

        f = np.zeros((len(atoms), 3))
        for k, atm_k in enumerate(syms):
            f0_k = self.debye.atomic_f0(atm_k)
            r_kj = pos[k] - pos[k + 1:]  # all vectors to k, r_kj for all j
            d_kj = np.sqrt((r_kj**2).sum(1))
            f0_j = self.debye.f0[k + 1:, :]  # f0s for all j
            for q, qq in enumerate(self.debye.qvec):
                A = self.Aq(q, s_sim, s_exp)
                qr = qq * d_kj
                F = (4 * A *
                     (f0_k[q] * f0_j[:, q] *
                      (np.cos(qr) - np.sin(qr) / qr)))[:, None] *\
                    (r_kj / (d_kj**2)[:, None])
                f[k + 1:] -= F
                f[k] += F.sum(0)

        return f


def forcetest():
    db = Debye()
    atoms = Atoms('Pt3', positions=[[0, 0, -1], [0, 0, 0], [0, 0, 1]])
    s_exp = db.debye(atoms)

    atoms.rattle(stdev=0.01, seed=1)

    atoms.calc = DebyeCalc(Debye(qvec=db.qvec), s_exp,
                           k=1e-8, sigma=db.qvec)

    F = atoms.get_forces()
    Fnum = atoms.calc.calculate_numerical_forces(atoms, d=1e-9)
    print('Numerical - Analytic Forces')
    print(Fnum - F)
    print(f'Numerical  Force Sum: {Fnum.sum():2.6f}')
    print(f'Analytical Force Sum: {F.sum():2.6f}')
